---
title: Current status
---

## nikita-noark5-core

[nikita-noark5-core](https://gitlab.com/OsloMet-ABI/nikita-noark5-core) is a 
free and open source implementation of the the 
[Noark 5 API](https://github.com/arkivverket/noark5-tjenestegrensesnitt-standard). 

As of June 19 we are currently at nikita version 0.4. A lot of the current 
efforts are focused on implementing the OData standard and working on ironing 
out inconsistencies in the API standard.

Nikita is being developed by a small development team and we are always interested 
in growing the number of contributors. If a free, open and stanardised approach 
to recordkeeping and preservation sounds interesting, join us. You can join us
on the IRC-channel #nikita on FreeNode (from a 
[browser](https://webchat.freenode.net?channels=#nikita)) or sign up the the 
mailing list
[_nikita-noark@nuug.no_](https://lists.nuug.no/mailman/listinfo/nikita-noark) 
hosted by [NUUG](https://www.nuug.no). 

